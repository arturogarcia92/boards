
DROP TABLE IF EXISTS `boards`;
DROP TABLE IF EXISTS `missions`;
DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `boards`
(
	`boardId` varchar(255),
	`boardTitle` varchar(255),

	PRIMARY KEY (`boardId`)
);

-- CREATE TABLE `mission`
-- (
-- 	`id` int,
-- 	`board_id` int,
-- 	`type` varchar(255),
-- 	`status` varchar(255),
-- 	`created_at` varchar(255),
-- 	PRIMARY KEY (`id`)
-- );
--
-- CREATE TABLE `task`
-- (
-- 	`id` int,
-- 	`mission_id` int,
-- 	`type` varchar(255),
-- 	`status` varchar(255),
-- 	`title` varchar(255),
-- 	`description` varchar(255),
-- 	`links` varchar(255),
-- 	`notes` varchar(255),
-- 	`created_at` varchar(255),
-- 	PRIMARY KEY (`id`)
-- );

-- ALTER TABLE `mission` ADD FOREIGN KEY (`board_id`) REFERENCES `board` (`id`);
--
-- ALTER TABLE `task` ADD FOREIGN KEY (`mission_id`) REFERENCES `mission` (`id`);

