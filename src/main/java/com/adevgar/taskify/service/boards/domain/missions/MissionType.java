package com.adevgar.taskify.service.boards.domain.missions;

public enum MissionType{
    LEARN, PROJECT
}
