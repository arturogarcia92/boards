package com.adevgar.taskify.service.boards.domain.tasks;


import com.adevgar.taskify.service.shared.domain.MissionId;
import com.adevgar.taskify.service.shared.domain.Status;
import com.adevgar.taskify.service.shared.domain.TaskId;

public class Task {

    //TODO añadir: Fecha creacion finalizacion, notificaciones programadas de emails

    private final TaskId taskId;
    private final MissionId missionId;
    private final TaskTitle taskTitle;
    private final TaskType taskType;
    private final TaskContent taskContent;
    private final Status status;
    private final TaskProgress taskProgress;
    private final TaskComment taskComment;

    public Task(TaskId taskId, MissionId missionId, TaskTitle taskTitle, TaskType taskType, TaskContent taskContent, Status status, TaskProgress taskProgress, TaskComment taskComment) {
        this.taskId = taskId;
        this.missionId = missionId;
        this.taskTitle = taskTitle;
        this.taskType = taskType;
        this.taskContent = taskContent;
        this.status = status;
        this.taskProgress = taskProgress;
        this.taskComment = taskComment;
    }


    public TaskId taskId() { return taskId; }

    public MissionId missionId() { return missionId; }

    public TaskTitle taskTitle() { return taskTitle; }

    public TaskType taskType() { return taskType; }

    public TaskContent taskNotes() { return taskContent; }

    public Status status() { return status; }

    public TaskProgress taskProgress() { return taskProgress; }

    public TaskComment taskComment() { return taskComment; }
}
