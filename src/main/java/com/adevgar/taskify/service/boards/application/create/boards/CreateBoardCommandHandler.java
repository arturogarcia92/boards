package com.adevgar.taskify.service.boards.application.create.boards;

import com.adevgar.taskify.service.boards.domain.boards.BoardTitle;
import com.adevgar.taskify.service.shared.domain.String20Char;
import org.springframework.stereotype.Component;

@Component
public class CreateBoardCommandHandler {

    BoardCreator boardCreator;


    public CreateBoardCommandHandler(BoardCreator boardCreator) {
        this.boardCreator = boardCreator;
    }

    public void create(CreateBoardCommand createBoardCommand){
         final BoardTitle boardTitle = new BoardTitle(new String20Char(createBoardCommand.getBoardTitle()));
         boardCreator.createBoard(boardTitle);
    }


}
