package com.adevgar.taskify.service.boards.domain.boards;


import com.adevgar.taskify.service.shared.domain.String20Char;




public class BoardTitle {


    String20Char boardTitle;
    //ver si no peta el setteo

    public BoardTitle(String20Char boardTitle) {
        this.boardTitle = boardTitle;
    }

    @Override
    public String toString() {
        return "BoardTitle{" +
                "boardTitle=" + boardTitle +
                '}';
    }

    public String value() {
        return boardTitle.value();
    }
}
