package com.adevgar.taskify.service.boards.domain.tasks;

public class TaskContent {

    private final String value;

    public TaskContent(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }



}
