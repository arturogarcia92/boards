package com.adevgar.taskify.service.boards.application.create.missions;

import com.adevgar.taskify.service.boards.application.create.boards.BoardCreator;
import com.adevgar.taskify.service.boards.domain.boards.Board;
import com.adevgar.taskify.service.boards.domain.missions.Mission;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MissionFinder {

    //TODO borrar este get, es para probar
    public List<Mission> getMissionDataByBoard(){ return new ArrayList(MissionCreator.missionsList.values()); }

}
