package com.adevgar.taskify.service.boards.persistence;

import com.adevgar.taskify.service.boards.domain.boards.Board;
import com.adevgar.taskify.service.boards.domain.missions.Mission;
import com.adevgar.taskify.service.boards.persistence.jpa.BoardDAO;
import com.adevgar.taskify.service.boards.persistence.jpa.BoardVO;
import com.adevgar.taskify.service.boards.persistence.jpa.MissionDAO;
import com.adevgar.taskify.service.boards.persistence.jpa.MissionVO;
import org.springframework.stereotype.Repository;

@Repository
public class MissionRepository {



    private final MissionDAO missionDAO;

    public MissionRepository(MissionDAO missionDAO) {
        this.missionDAO = missionDAO;
    }


    public void save (Mission mission){

        MissionVO missionVO = map(mission);

        missionDAO.save(missionVO);

    }

    private MissionVO map(Mission mission) {

        return new MissionVO(mission.missionId(), mission.boardId(), mission.missionTitle(), mission.missionType(), mission.status(), mission.missionProgress()  );
    }

}