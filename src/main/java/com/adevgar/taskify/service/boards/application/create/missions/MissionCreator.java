package com.adevgar.taskify.service.boards.application.create.missions;

import com.adevgar.taskify.service.boards.domain.missions.Mission;
import com.adevgar.taskify.service.boards.domain.missions.MissionProgress;
import com.adevgar.taskify.service.boards.domain.missions.MissionTitle;
import com.adevgar.taskify.service.boards.domain.missions.MissionType;
import com.adevgar.taskify.service.boards.persistence.MissionRepository;
import com.adevgar.taskify.service.shared.domain.BoardId;
import com.adevgar.taskify.service.shared.domain.MissionId;
import com.adevgar.taskify.service.shared.domain.Status;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class MissionCreator {

    //TODO quitar array inMemory e inyectarle la interfaz de un repository para hacer el create and save
    static Map<String,Mission> missionsList = new HashMap<>();
    private final MissionRepository missionRepository;

    public MissionCreator(MissionRepository missionRepository) {
        this.missionRepository = missionRepository;
    }


    //TODO debe recibir todos los atributos, ya que el handler llamara con todos lso attribs ej: commandHandler.
    public void createMission(BoardId boardId,
                              MissionType missionType,
                              MissionTitle missionTitle,
                              Status status,
                              MissionProgress missionProgress) {

        Mission mission = new Mission(boardId, generateUUID(), missionType, missionTitle, status, missionProgress);
        missionRepository.save(mission);
        missionsList.put(boardId.value(),mission);
    }

    private MissionId generateUUID(){ return (new MissionId(UUID.randomUUID().toString())); }

}
