package com.adevgar.taskify.service.boards.application.create.missions;


import com.adevgar.taskify.service.boards.application.create.boards.BoardCreator;
import com.adevgar.taskify.service.boards.application.create.boards.CreateBoardCommand;
import com.adevgar.taskify.service.boards.domain.boards.BoardTitle;
import com.adevgar.taskify.service.boards.domain.missions.MissionProgress;
import com.adevgar.taskify.service.boards.domain.missions.MissionTitle;
import com.adevgar.taskify.service.boards.domain.missions.MissionType;
import com.adevgar.taskify.service.shared.domain.BoardId;
import com.adevgar.taskify.service.shared.domain.Status;
import com.adevgar.taskify.service.shared.domain.String20Char;
import org.springframework.stereotype.Component;

@Component
public class CreateMissionCommandHandler {

    MissionCreator missionCreator;


    public CreateMissionCommandHandler(MissionCreator missionCreator) {
        this.missionCreator = missionCreator;
    }

    public void create(CreateMissionCommand createMissionCommand, String boardIdreq){

        final BoardId boardId = new BoardId(boardIdreq);
        final MissionType missionType =  MissionType.valueOf(createMissionCommand.getMissionType());
        final MissionTitle missionTitle = new MissionTitle(new String20Char(createMissionCommand.getMissionTitle()));
        final Status status = Status.valueOf(createMissionCommand.getStatus());
        final MissionProgress missionProgress = new MissionProgress(createMissionCommand.getMissionProgress());


        missionCreator.createMission(boardId,missionType,missionTitle,status,missionProgress);
    }


}
