package com.adevgar.taskify.service.boards.application.create.boards;

import com.adevgar.taskify.service.boards.domain.boards.Board;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoardFinder {

    //TODO borrar este get, es para probar
    public List<Board> getBoardData(){ return BoardCreator.boardList; }

}
