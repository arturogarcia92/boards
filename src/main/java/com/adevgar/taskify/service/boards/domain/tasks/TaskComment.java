package com.adevgar.taskify.service.boards.domain.tasks;

public class TaskComment {

    private final String value;

    public TaskComment(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }


}
