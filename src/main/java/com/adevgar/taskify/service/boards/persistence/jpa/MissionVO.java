package com.adevgar.taskify.service.boards.persistence.jpa;//package com.example.missionTasker.command.persistence.jpa;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MISSIONS")
public class MissionVO {

    @Id
    String missionid;
    String boardid;
    String missiontitle;
    String missiontype;
    String status;
    int missionprogress;

    public MissionVO(String missionid, String boardid, String missiontitle, String missiontype, String status, int missionprogress) {
        this.missionid = missionid;
        this.boardid = boardid;
        this.missiontitle = missiontitle;
        this.missiontype = missiontype;
        this.status = status;
        this.missionprogress = missionprogress;
    }

    public MissionVO() {
    }


    public String missionid() {
        return missionid;
    }

    public void setmissionid(String missionid) {
        this.missionid = missionid;
    }

    public String boardid() {
        return boardid;
    }

    public void setBoardid(String boardid) {
        this.boardid = boardid;
    }

    public String missiontitle() {
        return missiontitle;
    }

    public void setMissiontitle(String missiontitle) {
        this.missiontitle = missiontitle;
    }

    public String missiontype() {
        return missiontype;
    }

    public void setMissiontype(String missiontype) {
        this.missiontype = missiontype;
    }

    public String status() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int missionprogress() {
        return missionprogress;
    }

    public void setMissionprogress(int missionprogress) {
        this.missionprogress = missionprogress;
    }




}



