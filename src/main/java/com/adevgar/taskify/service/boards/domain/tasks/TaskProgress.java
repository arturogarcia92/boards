package com.adevgar.taskify.service.boards.domain.tasks;

public class TaskProgress {

    public int progress;

    public TaskProgress(int progress) {
        this.progress = progress;
    }

    public int progress() {
        return progress;
    }
}
