package com.adevgar.taskify.service.boards.persistence.jpa;//package com.example.missionTasker.command.persistence.jpa;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "BOARDS")
public class BoardVO {



    @Id
    String boardid;
    String boardtitle;

    public BoardVO() {
    }

    public String getBoardId() {
        return boardid;
    }

    public String getTitle() {
        return boardtitle;
    }

    public BoardVO(String boardid, String boardtitle) {
        this.boardid = boardid;
        this.boardtitle = boardtitle;
    }

    public void setBoardId(String boardId) {
        this.boardid = boardId;
    }

    public void setTitle(String title) {
        this.boardtitle = title;
    }
//    String description;
//    LocalDate createdAt;
}



