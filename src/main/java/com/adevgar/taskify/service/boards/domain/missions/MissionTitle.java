package com.adevgar.taskify.service.boards.domain.missions;

import com.adevgar.taskify.service.shared.domain.String20Char;

public class MissionTitle {


    String20Char missionTitle;

    public MissionTitle(String20Char missionTitle) {
        this.missionTitle = missionTitle;
    }


    public String value() {
        return missionTitle.value();
    }

}
