package com.adevgar.taskify.service.boards.infrastructure;

import com.adevgar.taskify.service.boards.application.create.CreateTaskCommandHandler;
import com.adevgar.taskify.service.boards.application.create.boards.CreateBoardCommand;
import com.adevgar.taskify.service.boards.application.create.missions.CreateMissionCommand;
import com.adevgar.taskify.service.boards.application.create.missions.CreateMissionCommandHandler;
import com.adevgar.taskify.service.boards.application.create.missions.MissionFinder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MissionController {

    CreateMissionCommandHandler createMissionCommandHandler;
    ObjectMapper om;
    MissionFinder missionFinder;


    public MissionController(CreateMissionCommandHandler createMissionCommandHandler, ObjectMapper om, MissionFinder missionFinder) {
        this.createMissionCommandHandler = createMissionCommandHandler;
        this.om = om;
        this.missionFinder = missionFinder;
    }


    @RequestMapping(value="/mission/{boardId}", method = RequestMethod.POST)
    public ResponseEntity<?> createBoard(@PathVariable String boardId, @RequestBody CreateMissionCommand request) {
        System.out.println(request.getMissionTitle()+" "+request.getStatus()+" "+request.getMissionType());
        createMissionCommandHandler.create(request, boardId);
        return ResponseEntity.ok().body(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/mission/{boardId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseBody
    public ResponseEntity<?>  getMissionByBoard() throws JsonProcessingException {
        return ResponseEntity.ok(om.writeValueAsString(missionFinder.getMissionDataByBoard()));
    }



}
