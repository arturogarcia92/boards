package com.adevgar.taskify.service.boards.infrastructure;

import com.adevgar.taskify.service.boards.application.create.boards.BoardFinder;
import com.adevgar.taskify.service.boards.application.create.boards.CreateBoardCommand;
import com.adevgar.taskify.service.boards.application.create.boards.CreateBoardCommandHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BoardController {


    //TODO -> Sacar al modelo de query el finder y a otro controller
    //TODO -> ponerme a crear tareas
    //TODO -> sacar configuracion de object mapper a otra clase configuration y hacerme otra clase para lanzar peticiones como response entities
    //TODO -> pensar en que logica meter para progresos y tal.

    ObjectMapper om;
    CreateBoardCommandHandler createBoardCommandHandler;
    BoardFinder boardFinder; //pasarlo al mundo de query

//    @Autowired
    BoardController(CreateBoardCommandHandler createBoardCommandHandler, BoardFinder boardFinder, ObjectMapper om){
        this.createBoardCommandHandler = createBoardCommandHandler;
        this.om = om;
        this.boardFinder = boardFinder;
    }


    @PostMapping(value="/board")
    public ResponseEntity<?>  createBoard(@RequestBody CreateBoardCommand request) {
        System.out.println(request.getBoardTitle());
        createBoardCommandHandler.create(request);
        return ResponseEntity.ok().body(HttpStatus.CREATED);
    }


    @RequestMapping(value = "/board", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseBody
    public ResponseEntity<?>  getAllBoards() throws JsonProcessingException {
        return ResponseEntity.ok(om.writeValueAsString(boardFinder.getBoardData()));
    }



}
