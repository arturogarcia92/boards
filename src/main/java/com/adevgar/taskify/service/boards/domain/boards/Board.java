package com.adevgar.taskify.service.boards.domain.boards;


import com.adevgar.taskify.service.boards.domain.missions.Mission;
import com.adevgar.taskify.service.shared.domain.BoardId;

import java.util.Collections;
import java.util.List;


public class Board {


//    @Autowired
    private final BoardId boardId;

    private final BoardTitle boardTitle;

    private List<Mission> missions = Collections.emptyList();

    public Board(BoardId boardId, BoardTitle boardTitle) {
        this.boardId = boardId;
        this.boardTitle = boardTitle;
    }

    public String boardId() { return boardId.value(); }

    public String boardTitle() { return boardTitle.value(); }

    public List<Mission> missions() { return missions; }

}
