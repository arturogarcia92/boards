package com.adevgar.taskify.service.boards.persistence.jpa;//package com.example.missionTasker.command.persistence.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface MissionDAO extends CrudRepository<MissionVO, String> {

    Optional<MissionVO> findById(String uuid);

//    List<BoardVO> findByBoardTitle(String title);
}