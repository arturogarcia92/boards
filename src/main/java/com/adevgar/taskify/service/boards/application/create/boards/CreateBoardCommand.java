package com.adevgar.taskify.service.boards.application.create.boards;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public class CreateBoardCommand {


    private String boardTitle;

    public CreateBoardCommand(String boardTitle) {
        this.boardTitle = boardTitle;
    }

    public CreateBoardCommand() { }

    //TODO ir añadiendo campos que faltan



    public String getBoardTitle() {
        return boardTitle;
    }
}


