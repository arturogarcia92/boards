package com.adevgar.taskify.service.boards.persistence.jpa;//package com.example.missionTasker.command.persistence.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface BoardDAO extends CrudRepository<BoardVO, String> {

    Optional<BoardVO> findById(String uuid);

//    List<BoardVO> findByBoardTitle(String title);
}