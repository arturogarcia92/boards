package com.adevgar.taskify.service.boards.application.create;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ObjectMapperConfiguration {

//    ObjectMapper om = configOm();

    @Bean
    public ObjectMapper ObjectMapperConfig() {
        return configOm();
    }


    ObjectMapper configOm(){
        ObjectMapper om = new com.fasterxml.jackson.databind.ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return om;
    }




}
