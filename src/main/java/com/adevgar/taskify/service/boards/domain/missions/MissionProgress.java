package com.adevgar.taskify.service.boards.domain.missions;

public class MissionProgress {

    public int progress;

    public MissionProgress(int progress) {
        this.progress = progress;
    }

    public int progress() {
        return progress;
    }

}
