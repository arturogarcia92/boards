package com.adevgar.taskify.service.boards.persistence;

import com.adevgar.taskify.service.boards.domain.boards.Board;
import com.adevgar.taskify.service.boards.persistence.jpa.BoardDAO;
import com.adevgar.taskify.service.boards.persistence.jpa.BoardVO;
import org.springframework.stereotype.Repository;

@Repository
public class BoardRepository {



    private final BoardDAO boardDAO;

    public BoardRepository(BoardDAO boardDAO) {
        this.boardDAO = boardDAO;
    }


    public void save(Board board){

        BoardVO boardVO = map(board);

        boardDAO.save(boardVO);

    }

    private BoardVO map(Board board) {

        return new BoardVO(board.boardId(), board.boardTitle());
    }

}