package com.adevgar.taskify.service.boards.application.create.boards;

import com.adevgar.taskify.service.boards.domain.boards.Board;
import com.adevgar.taskify.service.boards.domain.boards.BoardTitle;
import com.adevgar.taskify.service.boards.persistence.BoardRepository;
import com.adevgar.taskify.service.shared.domain.BoardId;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class BoardCreator {

    //TODO quitar array inMemory e inyectarle la interfaz de un repository para hacer el create and save
    static List<Board> boardList = new ArrayList<>();
    private final BoardRepository boardRepository;

    public BoardCreator(BoardRepository boardRepository) {
        this.boardRepository = boardRepository;
    }


    //TODO debe recibir todos los atributos, ya que el handler llamara con todos lso attribs ej: commandHandler.
    public void createBoard(BoardTitle boardTitle) {
        Board board = new Board(generateUUID(), boardTitle);
        boardRepository.save(board);
        boardList.add(board);
    }

    private BoardId generateUUID(){ return (new BoardId(UUID.randomUUID().toString())); }
}
