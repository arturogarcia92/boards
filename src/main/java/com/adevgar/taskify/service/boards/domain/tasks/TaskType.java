package com.adevgar.taskify.service.boards.domain.tasks;

public enum TaskType {
    VIDEO, ARTICLE, BOOK, NOTE
}
