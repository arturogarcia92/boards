package com.adevgar.taskify.service.boards.application.create.missions;

import com.adevgar.taskify.service.boards.domain.missions.MissionProgress;
import com.adevgar.taskify.service.boards.domain.missions.MissionTitle;
import com.adevgar.taskify.service.boards.domain.missions.MissionType;

public class CreateMissionCommand {

    private String missionTitle;
    private String missionType;
//    private String boardId;
    private String status;
    private int missionProgress;

    public CreateMissionCommand() {
    }

    public CreateMissionCommand(String missionTitle, String missionType, String status, int missionProgress) {
        this.missionTitle = missionTitle;
        this.missionType = missionType;
//        this.boardId = boardId;
        this.status = status;
        this.missionProgress = missionProgress;
    }

    public String getMissionTitle() {
        return missionTitle;
    }

    public String getMissionType() {
        return missionType;
    }

//    public String getBoardId() {
//        return boardId;
//    }

    public String getStatus() {
        return status;
    }

    public int getMissionProgress() {
        return missionProgress;
    }
}
