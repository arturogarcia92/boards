package com.adevgar.taskify.service.boards.domain.tasks;

import com.adevgar.taskify.service.shared.domain.String20Char;

public class TaskTitle {

    String20Char taskTitle;

    public TaskTitle(String20Char taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String value() {
        return taskTitle.value();
    }


}
