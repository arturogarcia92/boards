package com.adevgar.taskify.service.boards.domain.missions;

import com.adevgar.taskify.service.shared.domain.BoardId;
import com.adevgar.taskify.service.shared.domain.MissionId;
import com.adevgar.taskify.service.shared.domain.Status;


public class Mission {

    private final BoardId boardId;
    private final MissionId missionId;
    private final MissionType missionType;
    private final MissionTitle missionTitle;
    private final Status status;
    private final MissionProgress missionProgress;
//    private final List<Task> tasks;

    public Mission(BoardId boardId, MissionId missionId, MissionType missionType, MissionTitle missionTitle, Status status, MissionProgress missionProgress) {
        this.boardId = boardId;
        this.missionId = missionId;
        this.missionType = missionType;
        this.missionTitle = missionTitle;
        this.status = status;
        this.missionProgress = missionProgress;
//        this.tasks = tasks;
    }

    public String boardId() { return boardId.value(); }

    public String missionId() { return missionId.value(); }

    public String missionType() { return missionType.name(); }

    public String missionTitle() { return missionTitle.value(); }

    public String status() { return status.name(); }

    public int missionProgress() { return missionProgress.progress; }

//    public List<Task> tasks() { return tasks; }
}
