package com.adevgar.taskify.service.shared.domain;


public class String20Char {

    private final String value;

    public String20Char(String value) {
        this.value = validateValue(value);
    }

    private String validateValue(String text) {
        if(text.isEmpty()) throw new IllegalArgumentException("The text is empty");
        if(text.length()>20) throw new IllegalArgumentException("The text must be shorter than 20 chars and is "+text.length() );

        return text;
    }

    @Override
    public String toString() {
        return "String20Char{" +
                "value='" + value + '\'' +
                '}';
    }

    public String value() {
        return value;
    }
}
