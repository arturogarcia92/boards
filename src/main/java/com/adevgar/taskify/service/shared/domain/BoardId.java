package com.adevgar.taskify.service.shared.domain;


public class BoardId {


    private String value;



    public BoardId(String boardId) {
        this.value = boardId;
    }

    private void validateValue(String text) {
        if(text.isEmpty()) throw new IllegalArgumentException("The text is empty");
        if(text.length()>80) throw new IllegalArgumentException("The text is must be shorter than 30 chars");

        this.value = text;
    }

    public String value() {
        return value;
    }

}
