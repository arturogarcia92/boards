package com.adevgar.taskify.service.shared.domain;

public enum Status {
    TODO, IN_PROGRESS, DONE
}
