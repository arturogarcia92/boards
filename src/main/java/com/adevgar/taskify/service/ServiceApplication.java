package com.adevgar.taskify.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StopWatch;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@SpringBootApplication
@EnableSwagger2
public class ServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceApplication.class, args);
    }


    @Bean
    public Docket generateApiDocumentation() {
        Docket docket = this.generateDocket();
        return docket;
    }


    private Docket generateDocket() {
        return (new Docket(DocumentationType.SWAGGER_2)).forCodeGeneration(true).ignoredParameterTypes(new Class[]{Date.class}).directModelSubstitute(LocalDate.class, Date.class).directModelSubstitute(ZonedDateTime.class, java.util.Date.class).directModelSubstitute(LocalDateTime.class, java.util.Date.class).select().build();
    }
}
